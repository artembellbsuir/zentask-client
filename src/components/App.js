import React from "react";
import { Router, Switch, Route, Link, useLocation, useHistory, Redirect } from "react-router";
import MyBoards from "./MyBoards";
import { Board } from "./board/Board";
import { SnackbarProvider } from "notistack";
import { Provider } from "react-redux";
import { Header } from "./Header";
import { Register } from "./auth/Register";

import { Login } from "./auth/Login";
import { Fragment } from "react";
import { useSelector, useDispatch } from "react-redux";
import { loginSuccess } from "../redux/actions/authActions";
import { AuthenticatedApp } from "./auth/AuthenticatedApp";

export default function App() {
    const dispatch = useDispatch();

    const accessToken = useSelector((state) => state.auth.accessToken);
    // console.log("check token in App first...", accessToken);

    // React.useEffect(() => {
    //     console.log("redirect to home");
    // }, [accessToken]);

    // check here auth.isAuthorized, if not set => push ch

    const isAuthorized = useSelector((state) => state.auth.isAuthorized);

    // if (!accessToken) {
    // return <Login />;
    // history.push("/login");
    // }

    const boardRoute = isAuthorized ? <Route path="/boards/:boardId" component={Board} /> : <Redirect to="/login" />;

    return (
        <Layout>
            <Switch>
                <Route exact path="/register" component={Register} />
                <Route exact path="/login" component={Login} />
                <Route path="/" component={AuthenticatedApp} />

                {/* <Route path="/boards/:boardId" component={Board} /> */}
                {/* <BoardsRoute /> */}
                {/* <Route path="*" component={NoMatch} /> */}
            </Switch>
        </Layout>
    );
}

function BoardsRoute(props) {
    const isAuthorized = useSelector((state) => state.auth.isAuthorized);

    if (isAuthorized) {
        return <Route path="/boards/:boardId" component={Board} />;
    }
    return <Redirect to="/login" />;
}

function Layout({ children }) {
    let location = useLocation();
    return (
        <Fragment>
            {location.pathname !== "/login" && location.pathname !== "/register" ? <Header /> : null}
            {children}
        </Fragment>
    );
}

function NoMatch() {
    return (
        <div>
            <h3>
                No match for <code>{location.pathname}</code>
            </h3>
        </div>
    );
}
