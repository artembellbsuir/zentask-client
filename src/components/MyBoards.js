import React, { Fragment, useState, useEffect } from "react";
// import dynamic from "next/dynamic";
import { Link as RouterLink, useHistory } from "react-router-dom";

import Skeleton from "@material-ui/lab/Skeleton";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Typography } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Link from "@material-ui/core/Link";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import AddIcon from "@material-ui/icons/Add";

import { Header } from "./Header";
import store from "../redux/store";
import { addShortBoardInfos, fetchShortBoardInfos } from "../redux/actions/shortBoardInfosActions";
import { useSelector } from "react-redux";
import { useSnackbar } from "notistack";
import { connect, useDispatch } from "react-redux";
import boardsService from "../services/boardsService";

import CreateBoardModal from "./modals/CreateBoardModal";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        paddingTop: 75
    },
    paper: {
        padding: theme.spacing(1),
        color: theme.palette.text.secondary,
        height: 100
    },
    box: {
        height: "100%"
    },
    paperAdd: {
        padding: theme.spacing(1),
        color: theme.palette.text.secondary,
        height: 100
    }
}));

const MyBoards = () => {
    const { enqueueSnackbar } = useSnackbar();

    const dispatch = useDispatch();
    const [searchValue, setSearchValue] = useState("");
    const [showModal, setShowModal] = useState(false);
    const classes = useStyles();
    const { boards, status, error } = useSelector((state) => {
        return state.shortBoardInfos;
    });

    const { accessToken } = useSelector((state) => {
        return state.auth;
    });

    useEffect(() => {
        boardsService.getShortBoardsInfos().catch((e) => {
            enqueueSnackbar(e.message, { variant: "error" });
        });
    }, [accessToken]);

    const history = useHistory();

    function handleBoardClick(boardId, redirectUrl) {
        boardsService
            .getBoard(boardId)
            .then(() => {
                history.push(redirectUrl);
            })
            .catch((e) => {
                enqueueSnackbar(e.message, { variant: "error" });
            });
    }

    const addCardButton = (
        <Paper className={classes.paperAdd} onClick={() => setShowModal(true)}>
            <Box
                className={classes.box}
                alignItems="center"
                display="flex"
                p={1}
                bgcolor="background.paper"
                justifyContent="center"
            >
                <Typography>Create card</Typography>
                <AddIcon />
            </Box>
        </Paper>
    );

    const body =
        status === "idle" ? (
            <CircularProgress />
        ) : (
            <Grid container item xs={12} spacing={2}>
                {[...boards].map(([key, { id, title }]) => (
                    <Grid item xs={4} key={key}>
                        <div onClick={() => handleBoardClick(id, `/boards/${id}`)}>
                            <Paper className={classes.paper}>{title}</Paper>
                        </div>
                    </Grid>
                ))}

                <Grid item xs={4}>
                    {addCardButton}
                </Grid>
            </Grid>
        );

    return (
        <Fragment>
            {/* <Header handleSearchChange={(value) => setSearchValue(value)}/> */}
            <CreateBoardModal show={showModal} handleClose={() => setShowModal(false)} />

            <Container maxWidth="sm">
                <div className={classes.root}>
                    <Grid container spacing={2}>
                        {body}
                    </Grid>
                </div>
            </Container>
        </Fragment>
    );
};

export default MyBoards;
