import React from "react";

import { Router, Switch, Route, Link, useLocation, useHistory, Redirect } from "react-router";
import MyBoards from "../MyBoards";
import { history } from "../history";
import authService from "../../services/authService";
import { useSelector, useDispatch } from "react-redux";
import { Board } from "../board/Board";

export const AuthenticatedApp = () => {
    // console.log("render");
    const isAuthorized = authService.isAuthorized();

    if (isAuthorized) {
        return (
            <>
                <Route exact path="/" component={MyBoards} />
                <Route path="/boards/:boardId" component={Board} />
                {/* <BoardsRoute /> */}
                {/* <Route path="*" component={NoMatch} />{" "} */}
            </>
        );
    }
    history.push("/login");
    return null;
};
