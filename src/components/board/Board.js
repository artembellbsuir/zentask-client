import React, { useEffect, useState } from "react";
import { Fragment } from "react";
import { BoardList, BoardLists } from "./BoardList";
import { Header } from "../Header";
import { useParams } from "react-router-dom";
import { useSnackbar } from "notistack";

import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

import { BoardContext, defaultBoardContext } from "../../context/BoardContext";
import { useSelector, useDispatch } from "react-redux";
import boardsService from "../../services/boardsService";
import { selectList, selectBoard, selectCard } from "../../redux/actions/selectActions";

import CreateCardModal from "../modals/CreateCardModal";
import CreateListModal from "../modals/CreateListModal";

import DeleteBoardModal from "../modals/DeleteBoardModal";
import DeleteListModal from "../modals/DeleteListModal";
import DeleteCardModal from "../modals/DeleteCardModal";

import EditCardModal from "../modals/EditCardModal";
import BoardActions from "./BoardActions";

const Board = (props) => {
    // console.log('--->>>>>>');

    const dispatch = useDispatch();
    const { enqueueSnackbar } = useSnackbar();

    const [currentBoardContext, setCurrentBoardContext] = useState(defaultBoardContext);

    const [showCreateListModal, setShowCreateListModal] = useState(false);
    const [showCreateCardModal, setShowCreateCardModal] = useState(false);
    const [showBoardsActions, setShowBoardsActions] = useState(false);
    const [showEditCardModal, setShowEditCardModal] = useState(false);

    const [showDeleteBoardModal, setShowDeleteBoardModal] = useState(false);
    const [showDeleteListModal, setShowDeleteListModal] = useState(false);
    const [showDeleteCardModal, setShowDeleteCardModal] = useState(false);

    const { boardId } = useParams();

    const [selectedList, setSelectedList] = useState(null);
    const [selectedCard, setSelectedCard] = useState(null);

    const { board } = useSelector(
        ((boardId) => (state) => {
            const board = state.boardsInfo.get(parseInt(boardId));
            // const

            console.log(state.selected);

            return { board };
        })(boardId)
    );

    useEffect(() => {
        boardsService.getBoard(boardId).catch((e) => {
            enqueueSnackbar(e.message, { variant: "error" });
        });
    }, []);

    function onCardClick(column, card) {
        // setSelectedCard(card);
        // setSelectedList(column);
        // setShowEditCardModal(true);
    }

    function onCreateCardClick(list) {
        return () => {
            setSelectedList(list);
            setShowCreateCardModal(true);
            dispatch(selectList(list));
        };
        // add card and set focus on textarea
    }

    const toggleDrawer = (open) => (event) => {
        if (event.type === "keydown" && (event.key === "Tab" || event.key === "Shift")) {
            return;
        }

        setShowBoardsActions(open);
    };

    const [listActionsAnchor, setListActionsAnchor] = React.useState(null);

    function handleClickListActions(e, list) {
        setListActionsAnchor(e.currentTarget);
        dispatch(selectList(list));
    }

    function handleListDelete() {
        setShowDeleteListModal(true);
        setListActionsAnchor(null);
    }

    function handleListArchive() {
        console.log("LIST archive");
    }

    const [cardActionsAnchor, setCardActionsAnchor] = React.useState(null);
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleCardClick = (list) => (event, card) => {
        dispatch(selectList(list));
        dispatch(selectCard(card));
        setShowEditCardModal(true);
    };

    const handleCardActionsClick = (list) => (event, card) => {
        event.stopPropagation();
        dispatch(selectList(list));
        dispatch(selectCard(card));
        console.log(card);
        console.log("select LIISSSST");
        setAnchorEl(event.currentTarget);
    };

    function handleCardEdit() {
        onCardClick();
        setAnchorEl(false);
    }

    function handleCardDelete() {
        setAnchorEl(false);
    }

    function handleCardArchive() {
        setAnchorEl(false);
    }

    function handleCardDelete() {
        setShowDeleteCardModal(true);
        setAnchorEl(null);
    }

    function handleCardArchive() {
        console.log("Card archive");
    }

    const lists = board?.info?.lists ? board.info.lists : [];

    const body = !board?.info?.lists ? (
        <h1>Loading...</h1>
    ) : (
        <main className="columns">
            {[...lists].map(([listId, list]) => (
                <BoardList
                    key={listId}
                    column={list}
                    onCardClick={handleCardClick(list)}
                    onCardActionsClick={handleCardActionsClick(list)}
                    onCreateCardClick={onCreateCardClick(list)}
                    onListActionsClick={(e) => handleClickListActions(e, list)}
                />
            ))}

            <BoardList
                type="create"
                onCreateColumnClick={() => {
                    setShowCreateListModal(true);
                }}
            />
        </main>
    );

    return (
        <BoardContext.Provider value={currentBoardContext}>
            <Header onBoardPage handleBoardsActionsClick={toggleDrawer} />

            <BoardActions
                toggleDrawer={toggleDrawer}
                show={showBoardsActions}
                onDeleteBoardClick={() => {
                    setShowDeleteBoardModal(true);
                }}
            />

            <CreateCardModal
                list={selectedList}
                show={showCreateCardModal}
                handleClose={() => setShowCreateCardModal(false)}
            />
            <CreateListModal show={showCreateListModal} handleClose={() => setShowCreateListModal(false)} />

            <DeleteBoardModal show={showDeleteBoardModal} handleClose={() => setShowDeleteBoardModal(false)} />
            <DeleteListModal show={showDeleteListModal} handleClose={() => setShowDeleteListModal(false)} />
            <DeleteCardModal show={showDeleteCardModal} handleClose={() => setShowDeleteCardModal(false)} />

            <EditCardModal show={showEditCardModal} handleClose={() => setShowEditCardModal(false)} />

            {body}

            <Menu
                id="simple-list-menu"
                anchorEl={listActionsAnchor}
                keepMounted
                open={Boolean(listActionsAnchor)}
                onClose={() => setListActionsAnchor(null)}
            >
                <MenuItem onClick={handleListDelete}>Delete list</MenuItem>
                {/* <MenuItem onClick={handleListArchive}>Archive</MenuItem> */}
            </Menu>

            <Menu
                id="simple-card-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={() => setAnchorEl(null)}
            >
                {/* <MenuItem onClick={onCardClick}>Open card</MenuItem> */}
                <MenuItem onClick={handleCardDelete}>Delete card</MenuItem>
                {/* <MenuItem onClick={handleCardArchive}>Archive</MenuItem> */}
            </Menu>
        </BoardContext.Provider>
    );
};

export { Board };
