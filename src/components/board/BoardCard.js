// import Link from "next/link";
import { Fragment } from "react";

const BoardCard = ({ onCreateBoardClick, type, board }) => {
    if (type === "create") {
        return (
            <div onClick={() => onCreateBoardClick()} className="empty-board boards__item clickable-board">
                <h3 className="empty-board__text">Create board</h3>
            </div>
        );
    }




    const { id, title } = board;
    return (
        <Fragment>
            <div className="board boards__item clickable-board">
                <h3 className="board__name">{title}</h3>
                <Link href={`/boards/${id}`}>
                    <a className="clickable-board__link"></a>
                </Link>
            </div>
        </Fragment>
    );
};

export { BoardCard };
