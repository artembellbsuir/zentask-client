import React, { Fragment } from "react";
import { TaskCard } from "./TaskCard";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

import { BoardContext } from "../../context/BoardContext";

import MoreVertIcon from "@material-ui/icons/MoreVert";
import IconButton from "@material-ui/core/IconButton";

const BoardList = ({
    type,
    onCreateColumnClick,
    column,
    onCreateCardClick,
    onCardClick,
    onListActionsClick,
    onCardActionsClick
}) => {
    if (type === "create") {
        return (
            <div onClick={() => onCreateColumnClick()} className="create-column">
                <div className="add-card create-column__add-card">+ Add new column</div>
            </div>
        );
    }

    // create hook for such popups

    const { title, cards } = column.info;

    // console.log(column);
    return (
        <Fragment>
            <div className="column university-column">
                <h3 className="column__name">{title}</h3>

                <IconButton aria-label="settings" onClick={(e) => onListActionsClick(e)}>
                    <MoreVertIcon />
                </IconButton>

                <div className="cards column__cards">
                    {[...cards].map(([cardId, { info: card }]) => (
                        <TaskCard
                            onMenuClick={(e) => onCardActionsClick(e, card)}
                            onCardClick={(e) => onCardClick(e, card)}
                            key={cardId}
                            card={card}
                            column={column}
                        />
                    ))}
                </div>

                <div className="add-card column__add-card" onClick={() => onCreateCardClick(column)}>
                    + Add another card
                </div>
            </div>
        </Fragment>
    );
};

export { BoardList };
