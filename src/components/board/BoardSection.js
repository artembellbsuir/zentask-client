import React from "react";
import { BoardCard } from "./BoardCard";

const BoardSection = ({ onCreateBoardClick, children, title }) => {
    return (
        <div className="board-types__section my-boards">
            <h5 className="board-types__title">{title}</h5>
            <div className="boards">
                {children}
                <BoardCard onCreateBoardClick={() => onCreateBoardClick()} type="create" />
            </div>
        </div>
    );
};

export { BoardSection };
