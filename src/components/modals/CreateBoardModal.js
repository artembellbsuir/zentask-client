import React, { useState, useEffect } from "react";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import CircularProgress from "@material-ui/core/CircularProgress";

import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";

import store from "../../redux/store";
import { useDispatch, useSelector } from "react-redux";
import { addShortBoardInfoSuccess } from "../../redux/actions/shortBoardInfosActions";
import { useSnackbar } from "notistack";
import { createBoard } from "../../redux/actions/boardsActions";

import boardsService from "../../services/boardsService";

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        alignItems: "center"
    },
    wrapper: {
        margin: theme.spacing(1),
        position: "relative"
    },
    buttonSuccess: {
        backgroundColor: green[500],
        "&:hover": {
            backgroundColor: green[700]
        }
    },
    fabProgress: {
        color: green[500],
        position: "absolute",
        top: -6,
        left: -6,
        zIndex: 1
    },
    buttonProgress: {
        color: green[500],
        position: "absolute",
        top: "50%",
        left: "50%",
        marginTop: -12,
        marginLeft: -12
    }
}));

const CreateBoardModal = ({ show, handleClose }) => {
    const dispatch = useDispatch();
    const [title, setTitle] = useState("");
    // const [submitStatus, setSubmitStatus] = useState(null);
    const [errorMessage, setErrorMessage] = useState("Title field should not be empty.");

    const classes = useStyles();
    const [loading, setLoading] = useState(false);
    const [success, setSuccess] = useState(false);

    const buttonClassname = clsx({
        [classes.buttonSuccess]: success
    });

    const { status, error } = useSelector((state) => {
        return state.shortBoardInfos;
    });

    const { enqueueSnackbar } = useSnackbar();

    const handleClick = () => {
        enqueueSnackbar("I love snacks.");
    };

    const handleClickVariant = (variant) => () => {
        // variant could be success, error, warning, info, or default
        enqueueSnackbar("This is a success message!", { variant });
    };

    function handleSubmit(e) {
        e.preventDefault();

        if (!loading) {
            setSuccess(false);
            setLoading(true);
        }

        boardsService
            .createBoard(title)
            .then(() => {
                enqueueSnackbar("Successfully created new board!", { variant: "success" });
                handleClose();
            })
            .catch((error) => {
                enqueueSnackbar("Error happened trying to create board", { variant: "error" });
            });

        setTitle("");
        setErrorMessage("Title field should not be empty.");
        setLoading(false);
        setLoading(false);
    }

    function onTitleChange(e) {
        const newTitle = e.currentTarget.value;

        if (newTitle === "") {
            setErrorMessage("Title field should not be empty.");
        } else {
            setErrorMessage("");
        }

        setTitle(e.currentTarget.value);
    }

    return (
        <div>
            <Dialog open={show} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Create new board</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        If you want to create new board, enter it's title and click 'Save'
                    </DialogContentText>
                    <TextField
                        onChange={(e) => onTitleChange(e)}
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Board title"
                        type="text"
                        fullWidth
                        value={title}
                        error={errorMessage !== ""}
                        helperText={errorMessage}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    {/* <Button onClick={(e) => handleSubmit(e)} color="primary">
                        SDAsa
                    </Button> */}
                    <div className={classes.root}>
                        <div className={classes.wrapper}>
                            <Button
                                variant="contained"
                                color="primary"
                                className={buttonClassname}
                                disabled={errorMessage !== "" || loading}
                                onClick={handleSubmit}
                            >
                                Accept terms
                            </Button>
                            {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
                        </div>
                    </div>
                </DialogActions>
            </Dialog>
        </div>
    );
};

export default CreateBoardModal;
