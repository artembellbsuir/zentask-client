import React from "react";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

import { useSnackbar } from "notistack";
import cardService from "../../services/cardService";

const CreateCardModal = ({ list, show, handleClose }) => {
    const [title, setTitle] = React.useState("");
    const { enqueueSnackbar } = useSnackbar();


    function handleSubmit(e) {
        e.preventDefault();

        cardService
            .create(title)
            .then(() => {
                enqueueSnackbar("Successfully created new card!", { variant: "success" });
                handleClose();
            })
            .catch((error) => {
                enqueueSnackbar("Error happened trying to create new card", { variant: "error" });
            });

        handleClose();
    }

    return (
        <div>
            <Dialog open={show} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Create new card</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        If you want to create new card in list "{list?.title}"", enter it's title and click 'Save'
                    </DialogContentText>
                    <TextField
                        onChange={(e) => setTitle(e.currentTarget.value)}
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Card title"
                        type="text"
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={(e) => handleSubmit(e)} color="primary">
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
};

export default CreateCardModal;
