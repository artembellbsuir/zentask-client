import React from "react";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useSnackbar } from "notistack";
import listService from "../../services/listService";

const CreateListModal = ({ show, handleClose }) => {
    const [title, setTitle] = React.useState("");
    const { enqueueSnackbar } = useSnackbar();


    function handleSubmit(e) {
        e.preventDefault();
        listService
            .create(title)
            .then(() => {
                enqueueSnackbar("Successfully created new list!", { variant: "success" });
                handleClose();
            })
            .catch((error) => {
                enqueueSnackbar("Error happened trying to create new list", { variant: "error" });
            });
        handleClose();
    }

    return (
        <div>
            <Dialog open={show} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Create new list</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        If you want to create new list, enter it's title and click 'Save'
                    </DialogContentText>
                    <TextField
                        onChange={(e) => setTitle(e.currentTarget.value)}
                        autoFocus
                        margin="dense"
                        id="name"
                        label="List title"
                        type="text"
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={(e) => handleSubmit(e)} color="primary">
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
};

export default CreateListModal;
