import React from "react";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Typography from "@material-ui/core/Typography";

import boardService from "../../services/boardsService";
import { useSnackbar } from "notistack";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const DeleteBoardModal = ({ show, handleClose }) => {
    const board = useSelector(({ selected }) => selected.board);
    let history = useHistory();

    const { enqueueSnackbar } = useSnackbar();

    function handleSubmit(e) {
        e.preventDefault();
        const { id } = board;
        history.push("/");

        boardService
            .deleteBoard(id)
            .then(() => {
                enqueueSnackbar("Successfully deleted board", { variant: "success" });
                handleClose();
            })
            .catch((error) => {
                enqueueSnackbar("Error happened trying to delete board", { variant: "error" });
            });

        handleClose();
    }

    return (
        <div>
            <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={show}>
                <DialogTitle id="customized-dialog-title" onClose={handleClose}>
                    Delete board
                </DialogTitle>
                <DialogContent dividers>
                    <Typography gutterBottom>
                        After clicking 'Delete board' this board with all data on it will be discarded irreversibly.
                    </Typography>
                    <Typography gutterBottom>Are you sure you want to delete this board?</Typography>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleSubmit} color="secondary">
                        Delete board
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
};

export default DeleteBoardModal;
