import React from "react";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Typography from "@material-ui/core/Typography";

import { useSnackbar } from "notistack";
import cardService from "../../services/cardService";

const DeleteCardModal = ({ show, handleClose }) => {
    const { enqueueSnackbar } = useSnackbar();

    function handleSubmit(e) {
        e.preventDefault();

        console.log("OK.");

        cardService
            .deleteSelectedCard()
            .then(() => {
                enqueueSnackbar("Successfully deleted card", { variant: "success" });
                handleClose();
            })
            .catch((error) => {
                // console.log(error);
                enqueueSnackbar("Error happened trying to delete card", { variant: "error" });
            });

        handleClose();
    }

    return (
        <div>
            <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={show}>
                <DialogTitle id="customized-dialog-title" onClose={handleClose}>
                    Delete card
                </DialogTitle>
                <DialogContent dividers>
                    <Typography gutterBottom>
                        After clicking 'Delete card' this card be discarded irreversibly.
                    </Typography>
                    <Typography gutterBottom>Are you sure you want to delete this card?</Typography>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleSubmit} color="secondary">
                        Delete card
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
};

export default DeleteCardModal;
