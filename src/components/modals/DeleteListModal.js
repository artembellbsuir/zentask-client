import React from "react";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Typography from "@material-ui/core/Typography";

import { useSnackbar } from "notistack";

import listService from "../../services/listService";

const DeleteListModal = ({ show, handleClose }) => {
    const { enqueueSnackbar } = useSnackbar();

    function handleSubmit(e) {
        e.preventDefault();

        listService
            .delete()
            .then(() => {
                enqueueSnackbar("Successfully deleted list", { variant: "success" });
                handleClose();
            })
            .catch((error) => {
                enqueueSnackbar("Error happened trying to delete list", { variant: "error" });
            });
    }

    return (
        <div>
            <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={show}>
                <DialogTitle id="customized-dialog-title" onClose={handleClose}>
                    Delete list
                </DialogTitle>
                <DialogContent dividers>
                    <Typography gutterBottom>
                        After clicking 'Delete list' this list and all it's data be discarded irreversibly.
                    </Typography>
                    <Typography gutterBottom>Are you sure you want to delete this card?</Typography>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={(e) => handleSubmit(e)} color="secondary">
                        Delete list
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
};

export default DeleteListModal;
