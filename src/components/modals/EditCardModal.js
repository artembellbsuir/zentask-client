import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";

import TextField from "@material-ui/core/TextField";
import Slide from "@material-ui/core/Slide";
import cardService from "../../services/cardService";

import { useSnackbar } from "notistack";
import { useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
    appBar: {
        position: "relative"
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: "25ch"
    }
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const EditCardModal = ({ show, handleClose }) => {
    const classes = useStyles();

    const [editableCard, setEditableCard] = React.useState(null);
    const [textChanged, setTextChanged] = React.useState(false);

    const card = useSelector((state) => {
        const selectedCard = state.selected.card;
        return selectedCard;
    });

    function onTitleChange(e) {
        if (textChanged) {
            const title = e.target.value;
            setEditableCard({
                ...editableCard,
                title
            });
        } else {
            setTextChanged(true);
            setEditableCard(card);
        }
    }

    function onTextChange(e) {
        if (textChanged) {
            const text = e.target.value;
            console.log(text);
            setEditableCard({
                ...editableCard,
                text
            });
        } else {
            setTextChanged(true);
            setEditableCard(card);
        }
    }

    const { enqueueSnackbar } = useSnackbar();

    function handleSubmit(e) {
        e.preventDefault();
        if (textChanged) {
            cardService
                .updateCard(editableCard)
                .then(() => {
                    enqueueSnackbar("Successfully updated card", { variant: "success" });
                    handleClose();
                })
                .catch((error) => {
                    // console.log(error);
                    enqueueSnackbar("Error happened trying to update card", { variant: "error" });
                });
        }

        setTextChanged(false);
    }

    return (
        <div>
            <Dialog fullScreen open={show} onClose={handleClose} TransitionComponent={Transition}>
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="h6" className={classes.title}>
                            Edit cards
                        </Typography>
                        <Button autoFocus color="inherit" onClick={handleSubmit}>
                            Save
                        </Button>
                    </Toolbar>
                </AppBar>
                <TextField
                    label="Title"
                    // id="margin-none"
                    defaultValue={card?.title ?? ""}
                    className={classes.textField}
                    onChange={onTitleChange}
                />
                <TextField
                    label="Text"
                    id="margin-none"
                    defaultValue={card?.text ?? ""}
                    className={classes.textField}
                    onChange={onTextChange}
                />
            </Dialog>
        </div>
    );
};

export default EditCardModal;
