export const api = {
    BASE_RESTFUL_SERVER_HOSTNPORT: "http://localhost:3000",

    API_PREFIX: "api",
    API_CURRENT_VERSION: "v1",

    LOGIN: "credentials/login",
    REGISTER: "credentials/register",

    BOARDS: "boards",
    BOARD: "boards/{{boardId}}",
    BOARD_LISTS: "boards/{{boardId}}/lists",

    LISTS: "lists",
    LIST: "lists/{{listId}}",
    LIST_CARDS: "lists/{{listId}}/cards",

    CARDS: "cards",
    CARD: "cards/{{cardId}}"
};
