import { createContext } from "react";

const defaultBoardContext = {
    selectedBoard: null,
    selectedList: null,
    selectedCard: null
};
const BoardContext = createContext(defaultBoardContext);

export { BoardContext, defaultBoardContext };
