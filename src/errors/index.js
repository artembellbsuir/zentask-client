export class AnauthorizedError extends Error {
    constructor(msg) {
        super(msg);
    }
}

export class DefaultError extends Error {
    constructor(msg) {
        super(msg);
    }
}

