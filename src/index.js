import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import { SnackbarProvider } from "notistack";
import { Router } from "react-router";
import { Provider, useSelector } from "react-redux";

import App from "./components/App";
import store from "./redux/store";

import "./assets/styles/board.css";
import "./assets/styles/modal.css";

import { history } from "./components/history";

ReactDOM.render(
    <Provider store={store}>
        <SnackbarProvider maxSnack={3}>
            <Router history={history}>
                <App />
            </Router>
        </SnackbarProvider>
    </Provider>,
    document.getElementById("root")
);
