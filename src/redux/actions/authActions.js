import { api } from "../../constants/api";
// import authService from "../../services/authService";
import client from "../../services/client";
import { history } from "../../components/history";
import { DefaultError } from "../../errors/index";

export const LOGOUT_SUCCESS = "auth/logoutSuccess";
export const logoutSuccess = () => ({ type: LOGOUT_SUCCESS });
export const LOGIN_SUCCESS = "auth/loginSucccess";
export const loginSuccess = (tokens) => ({ type: LOGIN_SUCCESS, tokens });

export const login = (login, password) => (dispatch) => {
    return client
        .post(api.LOGIN, {}, { username: login, password }, {})
        .then(({ access_token: accessToken }) => {
            // !when logged in -> clear redux store
            // !lock routes on other boards, not belonging to user

            console.log(accessToken);
            localStorage.setItem("accessToken", accessToken);

            dispatch(loginSuccess({ accessToken }));
            return accessToken;
        })
        .catch((e) => {
            // console.log("qwe", e);
            throw e;
        });
};

export const register = (registerData) => (dispatch) => {
    return client
        .post(api.REGISTER, {}, registerData, {}, false)
        .then((responseText) => {
            console.log(responseText);

            return dispatch(login(registerData.username, registerData.password))
                .then((tokens) => {
                    dispatch(loginSuccess(tokens));
                    history.push("/");
                })
                .catch((e) => {
                    console.log(e);
                });
        })
        .catch((e) => {
            // if response is not ok
            if (e instanceof DefaultError) {
                console.log(e);
            } else {
                throw e;
            }
        });
};
