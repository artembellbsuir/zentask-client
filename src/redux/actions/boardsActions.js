import { api } from "../../constants/api";
import client from "../../services/socket-client";
import { addShortBoardInfoSuccess, addShortBoardInfoFailed, addShortBoardInfoStarted } from "./shortBoardInfosActions";
import { selectBoard } from "./selectActions";

// create board
export const CREATE_BOARD_SUCCESS = "boardInfos/createSuccess";
export const createBoardSuccess = (board) => ({ type: CREATE_BOARD_SUCCESS, board });
export const createBoard = (title) => (dispatch) => {
    dispatch(addShortBoardInfoStarted());

    client
        .post(api.BOARDS, {}, { title })
        .then((board) => {
            dispatch(addShortBoardInfoSuccess(board));
            dispatch(createBoardSuccess(board));
        })
        .catch((e) => {
            console.log("--.", e);
            dispatch(addShortBoardInfoFailed());
        });
};

// get board
export const GET_BOARD_STARTED = "boardInfos/getBoardStarted";
export const GET_BOARD_SUCCESS = "boardInfos/getBoardSuccess";
export const GET_BOARD_FAILED = "boardInfos/getBoardFailed";

export const getBoardStarted = (id) => ({ type: GET_BOARD_STARTED, id });
export const getBoardSucess = (board) => ({ type: GET_BOARD_SUCCESS, board });
export const getBoardFailed = (id) => ({ type: GET_BOARD_FAILED, id });
export const getBoard = (id) => (dispatch) => {
    dispatch(getBoardStarted());

    return client
        .get(api.BOARD, { boardId: id })
        .then((board) => {
            dispatch(selectBoard(board));
            dispatch(getBoardSucess(board));
        })
        .catch((e) => {
            console.log("--.", e);
            dispatch(getBoardFailed());
            throw e;
        });
};

// delete board
export const DELETE_BOARD_SUCCESS = "boardInfos/deleteBoardSuccess";
export const deleteBoardSucess = (id) => ({ type: DELETE_BOARD_SUCCESS, id });
export const deleteBoard = (id) => (dispatch) => {
    client
        .delete(api.BOARD, { boardId: id })
        // .then((response) => {
        //     console.log(response.ok);
        //     if (!response.ok) {
        //         throw new Error();
        //     } else {
        //         return response.json();
        //     }
        // })
        .then((board) => {
            dispatch(selectBoard(null));
            dispatch(deleteBoardSucess(board.id));
        })
        .catch((e) => {
            console.log("--.", e);
        });
};
