import { api } from "../../constants/api";
import client from "../../services/socket-client";

export const GET_CARD_SUCCESS = "cardInfos/getCardSuccess";
export const getCardSuccess = (list) => ({ type: GET_CARD_SUCCESS, list });

// create card
export const CREATE_LIST_CARDS_SUCCESS = "cardInfos/createCardsSuccess";
export const createCardsSuccess = (boardId, listId, cards) => ({
    type: CREATE_LIST_CARDS_SUCCESS,
    boardId,
    listId,
    cards
});
export const createCard = (title) => (dispatch, getState) => {
    const {
        // ! missing info
        board: { id: boardId },
        list: {
            info: { id: listId }
        }
    } = getState().selected;

    client
        .post(api.LIST_CARDS, { listId }, { title })
        // .then((response) => {
        //     if (!response.ok) {
        //         if (response.status === 401) {

        //         }
        //     } else {
        //         return response.json();
        //     }
        // })
        .then((list) => {
            console.log(list);
            const { cards } = list;
            dispatch(createCardsSuccess(boardId, listId, cards));
        })
        .catch((err) => console.log(err));
};

// delete card
export const DELETE_CARD_SUCCESS = "cardInfos/deleteCardSuccess";
export const deleteCardSuccess = (boardId, listId, cardId) => ({
    type: DELETE_CARD_SUCCESS,
    boardId,
    listId,
    cardId
});
export const deleteCard = () => (dispatch, getState) => {
    console.log("inDELETE");
    const {
        board: { id: boardId },
        list: {
            info: { id: listId }
        },
        card: { id: cardId }
    } = getState().selected;

    client
        .delete(api.CARD, { cardId })
        // .then((response) => response.json())
        .then(() => {
            dispatch(deleteCardSuccess(boardId, listId, cardId));
        })
        .catch((err) => console.log(err));
};

export const UPDATE_CARD_SUCCESS = "cards/updateCardSuccess";
export const updateCardSuccess = (boardId, listId, card) => ({ type: UPDATE_CARD_SUCCESS, boardId, listId, card });
export const updateCard = (updatedCard) => (dispatch, getState) => {
    const {
        board: { id: boardId },
        list: {
            info: { id: listId }
        },
        card: { id: cardId }
    } = getState().selected;

    console.log(updatedCard);

    client
        .put(api.CARD, { cardId }, { title: updatedCard.title, text: updatedCard.text })
        // .then((response) => response.json())
        .then((card) => {
            console.log(card);
            dispatch(updateCardSuccess(boardId, listId, card));
        })
        .catch((err) => console.log(err));
};
