import { api } from "../../constants/api";
import client from "../../services/socket-client";

export const GET_LIST_SUCCESS = "listInfos/getListSuccess";
export const getListSuccess = (list) => ({ type: GET_LIST_SUCCESS, list });

// create list
export const GET_BOARD_LISTS_SUCCESS = "listInfos/createListSuccess";
export const createListsSuccess = (boardId, lists) => ({ type: GET_BOARD_LISTS_SUCCESS, boardId, lists });
export const createList = (title) => (dispatch, getState) => {
    const { board } = getState().selected;
    client
        .post(api.BOARD_LISTS, { boardId: board.id }, { title })
        // .then((response) => response.json())
        .then((board) => {
            const { lists } = board;
            dispatch(createListsSuccess(board.id, lists));
        })
        .catch((err) => console.log(err));
};

// delete list
export const DELETE_LIST_SUCCESS = "listInfos/deleteListSuccess";
export const deleteListSuccess = (boardId, listId) => ({ type: DELETE_LIST_SUCCESS, boardId, listId });
export const deleteSelectedList = () => (dispatch, getState) => {
    const {
        board,
        list: {
            info: { id: listId }
        }
    } = getState().selected;

    client
        .delete(api.LIST, { listId })
        // .then((response) => response.json())
        .then(() => {
            dispatch(deleteListSuccess(board.id, listId));
        })
        .catch((err) => console.log(err));
};
