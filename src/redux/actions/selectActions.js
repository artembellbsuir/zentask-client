export const SELECT_LIST = "select/list";
export const SELECT_BOARD = "select/board";
export const SELECT_CARD = "select/card";

export const selectList = (list) => ({ type: SELECT_LIST, list });
export const selectBoard = (board) => ({ type: SELECT_BOARD, board });
export const selectCard = (card) => ({ type: SELECT_CARD, card });
