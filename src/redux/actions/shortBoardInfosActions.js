import { api } from "../../constants/api";
import client from "../../services/socket-client";
import { history } from "../../components/history";
import { DefaultError, AnauthorizedError } from "../../errors/index";
import { loginSuccess } from "./authActions";

// export const ADDED_ALL_SHORT_INFOS = "shortBoardInfos/addedAll";
// export const ADDED_ONE_SHORT_INFO = "shortBoardInfos/addedOne";
// export const addShortBoardInfo = (board) => ({ type: ADDED_ONE_SHORT_INFO, board });
// export const addShortBoardInfos = (boards) => ({ type: ADDED_ALL_SHORT_INFOS, boards });

export const GET_SHORT_BOARD_INFOS_STARTED = "shortBoardInfos/fetchStarted";
export const GET_SHORT_BOARD_INFOS_SUCCESS = "shortBoardInfos/fetchSuccess";
export const GET_SHORT_BOARD_INFOS_FAILED = "shortBoardInfos/fetchFailed";
export const ADD_SHORT_BOARD_INFO_SUCCESS = "shortBoardInfos/addInfoSuccess";
export const ADD_SHORT_BOARD_INFO_STARTED = "shortBoardInfos/addInfoStarted";
export const ADD_SHORT_BOARD_INFO_FAILED = "shortBoardInfos/addInfoFailed";

export const getShortBoardInfosStarted = () => ({ type: GET_SHORT_BOARD_INFOS_STARTED });
export const getShortBoardInfosSuccess = (boards) => ({ type: GET_SHORT_BOARD_INFOS_SUCCESS, boards });
export const getShortBoardInfosFailed = () => ({ type: GET_SHORT_BOARD_INFOS_FAILED });

export const addShortBoardInfoSuccess = (board) => ({ type: ADD_SHORT_BOARD_INFO_SUCCESS, board });
export const addShortBoardInfoStarted = () => ({ type: ADD_SHORT_BOARD_INFO_STARTED });
export const addShortBoardInfoFailed = () => ({ type: ADD_SHORT_BOARD_INFO_FAILED });

// make DI here for clients

export const fetchShortBoardInfos = () => (dispatch, getState) => {
    const { accessToken } = getState().auth;

    // console.log('-');
    // console.log(getState().auth);

    dispatch(getShortBoardInfosStarted());
    return client
        .get(api.BOARDS)
        .then((boards) => {
            console.log(boards);
            // dispatch(loginSuccess());
            dispatch(getShortBoardInfosSuccess(boards));
        })
        .catch((e) => {
            if (e instanceof DefaultError) {
                console.log("--.", e);
                dispatch(getShortBoardInfosFailed());
            } else {
                console.log(e);
                throw e;
            }
        });
};
