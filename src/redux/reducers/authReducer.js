import { LOGIN_SUCCESS, LOGOUT_SUCCESS } from "../actions/authActions";

// console.log(typeof window);
// console.log("in authReducer");

export const initialAuthState = {
    accessToken: null,
    refreshToken: null,
    isAuthorized: false
};

export const authReducer = (authState = initialAuthState, action) => {
    switch (action.type) {
        case LOGOUT_SUCCESS: {
            return {
                ...authState,
                isAuthorized: false
            };
        }
        case LOGIN_SUCCESS: {
            const { accessToken, refreshToken } = action.tokens;

            // console.log('s')
            // console.log(accessToken);

            return {
                ...authState,
                accessToken,
                isAuthorized: true
                // refreshToken
            };
        }
        default:
            return authState;
    }
};
