import { CREATE_BOARD_SUCCESS, DELETE_BOARD_SUCCESS, GET_BOARD_SUCCESS } from "../actions/boardsActions";
import { CREATE_LIST_CARDS_SUCCESS, DELETE_CARD_SUCCESS, UPDATE_CARD_SUCCESS } from "../actions/cardsActions";
import { DELETE_LIST_SUCCESS, GET_BOARD_LISTS_SUCCESS } from "../actions/listsActions";
import { listsReducer } from "./listsReducer";

import { Map } from "immutable";

const initialBoardsState = Map();
const initialBoardState = {
    info: {
        lists: Map()
    },
    status: "idle",
    error: null
};

export function boardsReducer(boardsState = initialBoardsState, action) {
    switch (action.type) {
        case CREATE_BOARD_SUCCESS: {
            const stateCopy = Map(boardsState);
            const { board } = action;

            return stateCopy.set(board.id, boardReducer(boardsState.get(board.id, initialBoardState), action));
        }
        case GET_BOARD_SUCCESS: {
            const { board } = action;
            const oldBoardState = boardsState.get(board.id, initialBoardState);
            const newBoardState = boardReducer(oldBoardState, action);
            return boardsState.set(board.id, newBoardState);
        }
        case DELETE_BOARD_SUCCESS: {
            const { id } = action;
            return boardsState.delete(id);
        }
        case GET_BOARD_LISTS_SUCCESS:
        case DELETE_LIST_SUCCESS:
        case DELETE_CARD_SUCCESS:
        case CREATE_LIST_CARDS_SUCCESS:
        case UPDATE_CARD_SUCCESS: {
            const { boardId } = action;
            return boardsState.set(boardId, boardReducer(boardsState.get(boardId, initialBoardState), action));
        }
        default:
            return boardsState;
    }
}

function boardReducer(boardState = initialBoardState, action) {
    switch (action.type) {
        case CREATE_BOARD_SUCCESS: {
            return {
                ...boardState,
                status: "loading"
            };
        }
        case CREATE_LIST_CARDS_SUCCESS:
        case DELETE_CARD_SUCCESS:
        case UPDATE_CARD_SUCCESS: {
            const oldListsState = boardState.info.lists;
            const newListsState = listsReducer(oldListsState, action);

            return {
                ...boardState,
                info: {
                    ...boardState,
                    lists: newListsState
                },
                status: "success",
                error: null
            };
        }
        case DELETE_LIST_SUCCESS: {
            const oldBoardState = { ...boardState };
            const oldListsState = boardState.info.lists;
            const newListsState = listsReducer(oldListsState, action);

            return {
                ...boardState,
                info: {
                    ...oldBoardState,
                    lists: newListsState
                },
                status: "success",
                error: null
            };
        }
        case GET_BOARD_SUCCESS: {
            const { board: boardInfo } = action;
            const lists = [...boardInfo.lists];

            const oldListsState = boardState.info.lists;
            const newAction = { lists, ...action };
            const newListsState = listsReducer(oldListsState, newAction);

            // -> immutable Record
            return {
                ...boardState,
                info: {
                    ...boardInfo,
                    lists: newListsState
                },
                status: "success",
                error: null
            };
        }
        case GET_BOARD_LISTS_SUCCESS: {
            const { lists } = action;
            const oldBoardState = { ...boardState };
            const oldListsState = boardState.info.lists;
            const newAction = { lists, ...action };
            const newListsState = listsReducer(oldListsState, newAction);

            // -> immutable Record
            return {
                ...boardState,
                info: {
                    ...oldBoardState,
                    lists: newListsState
                },
                status: "success",
                error: null
            };
        }
        default:
            return boardState;
    }
}
