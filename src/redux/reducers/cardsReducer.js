import { GET_BOARD_SUCCESS } from "../actions/boardsActions";
import { Map } from "immutable";
import { CREATE_LIST_CARDS_SUCCESS, DELETE_CARD_SUCCESS, UPDATE_CARD_SUCCESS } from "../actions/cardsActions";

export const initialCardsState = Map();
export const initialCardState = {
    info: {},
    status: "idle",
    error: null
};

export function cardsReducer(cardsState = initialCardsState, action) {
    switch (action.type) {
        case GET_BOARD_SUCCESS:
        case CREATE_LIST_CARDS_SUCCESS: {
            const { cards } = action;
            let newCardsState = Map(cardsState);
            for (let card of cards) {
                const oldCardState = cardsState.get(card.id, initialCardState);
                const newAction = { card, ...action };
                const newCardState = cardReducer(oldCardState, newAction);
                newCardsState = newCardsState.set(card.id, newCardState);
            }

            return newCardsState;
        }
        case DELETE_CARD_SUCCESS: {
            const { cardId } = action;
            return cardsState.delete(cardId);
        }
        case UPDATE_CARD_SUCCESS: {
            const {
                card: { id: cardId }
            } = action;
            return cardsState.set(cardId, cardReducer(cardsState.get(cardId), action));
        }
        default:
            return cardsState;
    }
}

export function cardReducer(cardState = initialCardState, action) {
    switch (action.type) {
        case CREATE_LIST_CARDS_SUCCESS:
        case GET_BOARD_SUCCESS:
        case UPDATE_CARD_SUCCESS: {
            const { card } = action;
            return {
                ...cardState,
                info: card,
                status: "success",
                error: null
            };
        }
        default:
            return cardState;
    }
}
