import { combineReducers } from "redux";

import { shortBoardInfosReducer } from "./shortBoardInfosReducer";
import { boardsReducer } from "./boardsReducer";
import { selected } from "./selectedReducer";
import { authReducer } from "./authReducer";

const rootReducer = combineReducers({
    shortBoardInfos: shortBoardInfosReducer,
    boardsInfo: boardsReducer,
    selected: selected,
    auth: authReducer
});

export default rootReducer;
