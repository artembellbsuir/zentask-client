import { Map } from "immutable";
import { GET_BOARD_SUCCESS } from "../actions/boardsActions";
import { DELETE_LIST_SUCCESS, GET_BOARD_LISTS_SUCCESS } from "../actions/listsActions";
import { CREATE_LIST_CARDS_SUCCESS, DELETE_CARD_SUCCESS, UPDATE_CARD_SUCCESS } from "../actions/cardsActions";
import { cardReducer, cardsReducer, initialCardState } from "./cardsReducer";

export const initialListsState = Map();
export const initialListState = {
    info: {
        cards: Map()
    },
    status: "idle",
    error: null
};

export function listsReducer(listsState = initialListsState, action) {
    switch (action.type) {
        case GET_BOARD_SUCCESS:
        // existing lists are overwriten - get only one new list
        case GET_BOARD_LISTS_SUCCESS: {
            const { lists } = action;

            let newListsState = Map(listsState);
            for (let list of lists) {
                const oldListState = listsState.get(list.id, initialListState);
                const newAction = { list, ...action };
                const newListState = listReducer(oldListState, newAction);
                newListsState = newListsState.set(list.id, newListState);
            }

            return newListsState;
        }
        case DELETE_LIST_SUCCESS: {
            const { listId } = action;
            console.log(listsState.delete(listId));
            return listsState.delete(listId);
        }
        case CREATE_LIST_CARDS_SUCCESS:
        case DELETE_CARD_SUCCESS:
        case UPDATE_CARD_SUCCESS: {
            const { listId } = action;
            const oldListState = listsState.get(listId);
            const newListState = listReducer(oldListState, action);
            return listsState.set(listId, newListState);
        }
        default:
            return listsState;
    }
}

export function listReducer(listState = initialListState, action) {
    switch (action.type) {
        case GET_BOARD_LISTS_SUCCESS:
        case GET_BOARD_SUCCESS: {
            const { list: listInfo } = action;
            const cards = [...listInfo.cards];
            const oldCardsState = listState.info.cards;
            const newAction = { cards, ...action };
            const newCardsState = cardsReducer(oldCardsState, newAction);

            return {
                ...listState,
                info: {
                    ...listInfo,
                    cards: newCardsState
                },
                status: "success",
                error: null
            };
        }
        case CREATE_LIST_CARDS_SUCCESS: {
            const { cards: listCards } = action;
            const cards = [...listCards];
            const oldCardsState = listState.info.cards;
            const newAction = { cards, ...action };
            const newCardsState = cardsReducer(oldCardsState, newAction);

            return {
                ...listState,
                info: {
                    ...listState.info,
                    cards: newCardsState
                },
                status: "success",
                error: null
            };
        }
        case DELETE_CARD_SUCCESS:
        case UPDATE_CARD_SUCCESS: {
            const oldCardsState = listState.info.cards;
            const newCardsState = cardsReducer(oldCardsState, action);

            return {
                ...listState,
                info: {
                    ...listState.info,
                    cards: newCardsState
                },
                status: "success",
                error: null
            };
        }
        default:
            return listState;
    }
}
