import { SELECT_BOARD, SELECT_CARD, SELECT_LIST } from "../actions/selectActions";

const initialState = {
    board: null,
    list: null,
    card: null
};

export function selected(state = initialState, action) {
    switch (action.type) {
        case SELECT_LIST: {
            console.log(SELECT_LIST);
            const { list } = action;
            return {
                ...state,
                list
            };
        }
        case SELECT_BOARD: {
            console.log(SELECT_BOARD);
            const { board } = action;
            console.log(board);
            return {
                ...state,
                board
            };
        }
        case SELECT_CARD: {
            console.log(SELECT_CARD);
            const { card } = action;
            console.log(card);
            return {
                ...state,
                card
            };
        }
        default:
            return state;
    }
}
