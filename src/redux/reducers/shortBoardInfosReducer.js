import { DELETE_BOARD_SUCCESS } from "../actions/boardsActions";
import {
    GET_SHORT_BOARD_INFOS_SUCCESS,
    GET_SHORT_BOARD_INFOS_STARTED,
    GET_SHORT_BOARD_INFOS_FAILED,
    ADD_SHORT_BOARD_INFO_SUCCESS,
    ADD_SHORT_BOARD_INFO_STARTED,
    ADD_SHORT_BOARD_INFO_FAILED
} from "../actions/shortBoardInfosActions";

const initialState = {
    boards: new Map(),
    status: "idle",
    error: null
};

export function shortBoardInfosReducer(state = initialState, action) {
    switch (action.type) {
        case GET_SHORT_BOARD_INFOS_STARTED: {
            return {
                ...state,
                status: "loading"
            };
        }
        case GET_SHORT_BOARD_INFOS_SUCCESS: {
            const boards = new Map();

            action.boards.forEach((board) => {
                boards.set(board.id, board);
            });

            return {
                ...state,
                boards,
                status: "success",
                error: null
            };
        }
        case GET_SHORT_BOARD_INFOS_FAILED: {
            return {
                ...state,
                status: "failed",
                error: "Error coole yea!"
            };
        }
        case ADD_SHORT_BOARD_INFO_SUCCESS: {
            const newBoards = new Map(state.boards);
            const { board } = action;
            newBoards.set(board.id, board);

            return {
                ...state,
                boards: newBoards,
                status: "success",
                error: null
            };
        }

        case ADD_SHORT_BOARD_INFO_STARTED: {
            return {
                ...state,
                status: "loading",
                error: null
            };
        }

        case ADD_SHORT_BOARD_INFO_FAILED: {
            return {
                ...state,
                status: "failed",
                error: "Error creating new board."
            };
        }
        case DELETE_BOARD_SUCCESS: {
            const newBoards = new Map(state.boards);
            newBoards.delete(action.id);
            return {
                ...state,
                boards: newBoards
            };
        }
        default:
            return state;
    }
}
