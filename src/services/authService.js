import { api } from "../constants/api";
import client from "./client";
import store from "../redux/store";
import * as authActions from "../redux/actions/authActions";

class AuthService {
    isAuthorized() {
        let { accessToken } = store.getState().auth;
        if (!accessToken) {
            return this.authorizeWithLocalToken();
        }
        return accessToken !== null;
    }

    authorizeWithLocalToken() {
        const localAccessToken = localStorage.getItem("accessToken");
        if (localAccessToken) {
            store.dispatch(authActions.loginSuccess({ accessToken: localAccessToken }));
            return true;
        }
        return false;
    }

    async login(username, password) {
        return store.dispatch(authActions.login(username, password));
    }

    async register(registerData) {
        return store.dispatch(authActions.register(registerData));
    }
}

export default new AuthService();
