import store from "../redux/store";
import { createBoard, getBoard, deleteBoard } from "../redux/actions/boardsActions";
import { fetchShortBoardInfos } from "../redux/actions/shortBoardInfosActions";

class BoardsService {
    async createBoard(title) {
        return store.dispatch(createBoard(title));
    }

    async deleteBoard(id) {
        return store.dispatch(deleteBoard(id));
    }

    async getBoard(id) {
        return store.dispatch(getBoard(id));
    }

    async getShortBoardsInfos() {
        return store.dispatch(fetchShortBoardInfos());
    }
}

export default new BoardsService();
