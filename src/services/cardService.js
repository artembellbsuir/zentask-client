import { createCard, deleteCard, updateCard } from "../redux/actions/cardsActions";
import store from "../redux/store";

class CardService {
    // async archive(id) {
    //     return store.dispatch(archiveCard(id));
    // }

    async deleteSelectedCard() {
        return store.dispatch(deleteCard());
    }

    async create(title) {
        return store.dispatch(createCard(title));
    }

    async updateCard(card) {
        return store.dispatch(updateCard(card));
    }
}

export default new CardService();
