import { api } from "../constants/api";
import mustache from "mustache";
import store from "../redux/store";
import { AnauthorizedError, DefaultError } from "../errors/index";
import { history } from "../components/history";
import { logoutSuccess } from "../redux/actions/authActions";
import axios from "axios";

const resolve = (path) => {
    return `${api.BASE_RESTFUL_SERVER_HOSTNPORT}/${api.API_PREFIX}/${api.API_CURRENT_VERSION}/${path}`;
};

class Client {
    get(url, urlParams = {}, otherParams = {}, shouldParseJson = true, needAuthentication = true) {
        const resultUrl = resolve(mustache.render(url, urlParams));

        let { accessToken } = store.getState().auth;

        if (!accessToken) {
            accessToken = localStorage.getItem("accessToken");
            // localStorage.setItem("accessToken", accessToken);
        }

        console.log(accessToken);

        // accessToken =
        //     "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFydGVtYmVsbHEiLCJ1c2VySWQiOjMsImlhdCI6MTYxOTM0OTgwMSwiZXhwIjozNjAxNjE5MzQ5ODAxfQ.qhoUoZUs1WYc4eS7YfkyoGVCqRnue3RkQeMo1Hdo4b4";

        console.log(accessToken);
        return axios
            .get(resultUrl, {
                headers: {
                    Authorization: `Bearer ${accessToken}`
                }
            })
            .then((response) => {
                if (!response.statusText) {
                    if (response.statusText === 401) {
                        store.dispatch(logoutSuccess());
                        // add errors for catching in components
                        history.push("/login");
                        throw new AnauthorizedError("You are unauthorized. Enter your credentials again");
                    }
                    console.log(response);
                    // console.log(response.ok);
                    throw new DefaultError("Some error happened with your response");
                }
                if (shouldParseJson) {
                    return response.data;
                }
                return response.text();
            });
    }

    post(url, urlParams = {}, body = {}, otherParams = {}, shouldParseJson = true, needAuthentication = false) {
        const resultUrl = resolve(mustache.render(url, urlParams));
        console.log(resultUrl);
        console.log(body);

        return axios.post(resultUrl, body).then((response) => {
            // console.log(response.);
            // change the way i handle this statusText
            if (!response.statusText) {
                if (response.status === 401) {
                    // add errors for catching in components
                    history.push("/login");
                    throw new AnauthorizedError("You are unauthorized. Enter your credentials again");
                } else if (response.status === 403) {
                    throw new DefaultError("You have entered incorrect credentials.");
                }
                throw new DefaultError("Some error happened with your response");
            }
            if (shouldParseJson) {
                return response.data;
            }
            return response.text();
        });
    }

    // add shouldParseJson
    put(url, urlParams = {}, body = {}, otherParams = {}, needAuthentication = false) {
        const resultUrl = resolve(mustache.render(url, urlParams));

        // return fetch(resultUrl, {
        //     method: "PUT",
        //     body: JSON.stringify(body),
        //     credentials: "include",
        //     ...otherParams
        // })

        return axios.put(resultUrl, body).then((response) => {
            if (!response.statusText) {
                console.log(response.status);
                if (response.status === 401) {
                    // add errors for catching in components
                    history.push("/login");
                    throw new AnauthorizedError("You are unauthorized. Enter your credentials again");
                }
                throw new DefaultError("Some error happened with your response.");
            }
            return response.data;
        });
    }

    delete(url, urlParams = {}, otherParams = {}, needAuthentication = false) {
        const resultUrl = resolve(mustache.render(url, urlParams));
        console.log(resultUrl);

        // return fetch(resultUrl, {
        //     method: "DELETE",
        //     credentials: "include",
        //     ...otherParams
        // })
        return axios.delete(resultUrl).then((response) => {
            if (!response.statusText) {
                if (response.status === 401) {
                    // add errors for catching in components
                    history.push("/login");
                    throw new AnauthorizedError("You are unauthorized. Enter your credentials again");
                }
                throw new DefaultError("Some error happened with your response");
            }
            return response.data;
        });
    }
}

export default new Client();
