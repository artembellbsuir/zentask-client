import { createList, deleteSelectedList } from "../redux/actions/listsActions";
import store from "../redux/store";

class ListService {
    // async archive(id) {
        // return store.dispatch(archiveList(id));
    // }

    async delete(id) {
        return store.dispatch(deleteSelectedList(id));
    }

    async create(title) {
        return store.dispatch(createList(title));
    }    
}

export default new ListService();
