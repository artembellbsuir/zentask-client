import { api } from "../constants/api";
import mustache from "mustache";
import store from "../redux/store";
import { AnauthorizedError, DefaultError } from "../errors/index";
import { history } from "../components/history";
import { logoutSuccess } from "../redux/actions/authActions";
import axios from "axios";
import io from "socket.io-client";
// import "./socket.io.js";
// import io from "socket.io-client/dist/socket.io";

const resolve = (path) => {
    return `${api.BASE_RESTFUL_SERVER_HOSTNPORT}/${api.API_PREFIX}/${api.API_CURRENT_VERSION}/${path}`;
};

class SocketClient {
    constructor() {
        this.socket = io(
            "http://localhost:3000"
            // {
            // path: "/"
            // }
        );

        this.socket.on("connect", () => {
            console.log("CONNECTED" + this.socket.id);
        });
        this.socket.on("disconnect", () => {
            console.log("disconnect");
        });
        this.socket.on("test", (data) => {
            console.log(`Test: ${data}`);
        });
    }

    async get(url, urlParams = {}, otherParams = {}, shouldParseJson = true, needAuthentication = true) {
        const resultUrl = `get/${url}`;
        console.log(resultUrl);
        console.log(urlParams);

        return new Promise((resolve, reject) => {
            let { accessToken } = store.getState().auth;
            if (!accessToken) {
                accessToken = localStorage.getItem("accessToken");
            }

            try {
                this.socket.emit(
                    resultUrl,
                    {
                        ...urlParams,
                        accessToken
                    },
                    (response) => {
                        resolve(response);
                    }
                );
            } catch (e) {
                reject(e);
                console.log(e);
            }
            // return Promise.resolve([]);
        });
    }

    post(url, urlParams = {}, body = {}, otherParams = {}, shouldParseJson = true, needAuthentication = false) {
        const resultUrl = `post/${url}`;
        console.log(resultUrl);
        console.log(urlParams);

        return new Promise((resolve, reject) => {
            let { accessToken } = store.getState().auth;
            if (!accessToken) {
                accessToken = localStorage.getItem("accessToken");
            }

            try {
                this.socket.emit(
                    resultUrl,
                    {
                        ...urlParams,
                        ...body,
                        accessToken
                    },
                    (response) => {
                        resolve(response);
                    }
                );
            } catch (e) {
                reject(e);
                console.log(e);
            }
            // return Promise.resolve([]);
        });
    }

    put(url, urlParams = {}, body = {}, otherParams = {}, needAuthentication = false) {}

    delete(url, urlParams = {}, otherParams = {}, needAuthentication = false) {
        const resultUrl = `delete/${url}`;
        console.log(resultUrl);
        console.log(urlParams);

        return new Promise((resolve, reject) => {
            let { accessToken } = store.getState().auth;
            if (!accessToken) {
                accessToken = localStorage.getItem("accessToken");
            }

            try {
                this.socket.emit(
                    resultUrl,
                    {
                        ...urlParams,
                        accessToken
                    },
                    (response) => {
                        resolve(response);
                    }
                );
            } catch (e) {
                reject(e);
                console.log(e);
            }
            // return Promise.resolve([]);
        });
    }
}

export default new SocketClient();
